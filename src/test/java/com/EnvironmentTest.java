package com;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import static java.lang.Thread.sleep;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by ealtuhova on 1/14/2017.
 */
public class EnvironmentTest {
    public static void main(String [] args){
        WebDriver driver = new ChromeDriver();

        //Launch the Online Store Website
        driver.get("http://localhost/opencart");


        //Css Selectors

//        WebElement element = driver.findElement(By.cssSelector("html body header div div div div a img"));
//        Assert.assertTrue(element.isDisplayed());
//        element.click();
//
//        element = driver.findElement(By.cssSelector("#cart > button"));
//        Assert.assertTrue(element.isDisplayed());
//        element.click();
//
//        element = driver.findElement(By.cssSelector("#cart > ul > li > p"));
//        Assert.assertThat(element.getText(), is("Your shopping cart is empty!"));

        //Different types of Selectors

//        WebElement element = driver.findElement(By.id("cart"));
//        Assert.assertTrue(element.isDisplayed());
//        element.click();
//
//        WebElement element2 = driver.findElement(By.className("text-center"));
//        Assert.assertTrue(element2.isDisplayed());
//        Assert.assertThat(element2.getText(), is("Your shopping cart is empty!"));
//        element.click();
//
//        element = driver.findElement(By.linkText("MP3 Players"));
//        Assert.assertTrue(element.isDisplayed());
//        Actions builder = new Actions(driver);
//        builder.moveToElement(element).build().perform();
//
//        element = driver.findElement(By.partialLinkText("Show All"));
//        Assert.assertTrue(element.isDisplayed());
//        element.click();

        //XPath selectors

        WebElement element = driver.findElement(By.xpath("/html/body/footer/div/div/div[2]/ul/li[3]/a"));
        Assert.assertTrue(element.isDisplayed());
        element.click();

        element = driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/ul/li[2]/a"));
        Assert.assertTrue(element.isDisplayed());
        element.click();

        element = driver.findElement(By.xpath("/html/body/div[2]//input"));
        Assert.assertTrue(element.isDisplayed());
        element.sendKeys("test@email.com");

        element = driver.findElement(By.xpath("//div[2]/input"));
        Assert.assertTrue(element.isDisplayed());
        element.sendKeys("testpassword");

        element = driver.findElement(By.xpath("//*[text()='Forgotten Password']"));
        Assert.assertTrue(element.isDisplayed());
        element.click(); //*[@id="content"]/div/div[2]/div/form/div[2]/a

        element = driver.findElement(By.xpath("//*[@placeholder=\"E-Mail Address\"]"));
        Assert.assertTrue(element.isDisplayed());
        element.sendKeys("test.email@email.com");

//        // Print a Log In message to the screen
//        System.out.println("Finally! We are ready to go further!");
//        Assert.assertThat(driver.getCurrentUrl(), is("http://localhost/opencart/index.php?route=product/category&path=34"));
//        // Close the driver
//
        driver.close();
    }
}
