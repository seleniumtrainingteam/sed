package com;

import com.openCart.PageObjects.RegisterUser;
import com.openCart.PageObjects.RegisterUserSuccess;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by ealtuhova on 2/13/2017.
 */
public class PageObjectTest {

    @Test
    public void registerNewUser(){
        WebDriver driver = new ChromeDriver();
        RegisterUser pg = new RegisterUser(driver);

        pg.getFirstName().sendKeys("Elena");
        pg.getLastName().sendKeys("Altuhova1232256889");
        pg.getEmail().sendKeys("elena.altuhova2233678@gmail.com");
        pg.getTelephone().sendKeys("+340958340985034");
        pg.getAddress1().sendKeys("str. Sfatul Tarii 15");
        pg.getCity().sendKeys("Chisinau");
        pg.getPostcode().sendKeys("MD-2075");
        pg.getCountry().sendKeys("Moldova");
        pg.getRegion().sendKeys("Chisinau");

        pg.getPassword().sendKeys("Abcd1234!");
        pg.getPassConfirm().sendKeys("Abcd1234!");

        pg.getLicense().click();

        pg.getSubmit().click();

        RegisterUserSuccess pgs = new RegisterUserSuccess(driver);
        Assert.assertThat(pgs.getConfirmationMessage().getText(), is("Your Account Has Been Created!"));
        pgs.getContinueButton().click();
        Assert.assertThat("http://localhost/opencart/index.php?route=account/account", is(driver.getCurrentUrl()));

        driver.close();
    }
}
