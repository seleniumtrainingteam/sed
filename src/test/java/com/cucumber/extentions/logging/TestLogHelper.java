package com.cucumber.extentions.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Created by vdrumea on 1/11/2017.
 */
public class TestLogHelper {
    public static final String TEST_NAME = "testname";
    private static final Logger log = LoggerFactory.getLogger(TestLogHelper.class);
    public static String CURRENT_LOG_NAME = "";

    public static void startTestLogging(String name) throws Exception {
        MDC.put(TEST_NAME, name);
        CURRENT_LOG_NAME = name;
    }

    public static String stopTestLogging() {
        String name = MDC.get(TEST_NAME);
        MDC.remove(TEST_NAME);
        return name;
    }

    public static String getCurrentLogName(){
        if (MDC.get(TEST_NAME)==null){
            return  "test";
        }
        return MDC.get(TEST_NAME);
    }
}
