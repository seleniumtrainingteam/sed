package com.cucumber.extentions.reporting;

import ch.qos.logback.core.AppenderBase;

/**
 * Created by vdrumea on 1/11/2017.
 */
public class CukeAppender extends AppenderBase {

    @Override
    protected void append(Object o) {
        CukeScenarioContext.getInstance().getScenario().write(sanitizeForHTML(o));
    }

    private String sanitizeForHTML(Object o) {
        String text = o.toString();
        text = text.replaceAll("<", "&lt;");
        text = text.replaceAll(">", "&gt;");

        return text;
    }
}
