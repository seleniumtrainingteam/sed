package com.cucumber.extentions.reporting;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

/**
 * Created by vdrumea on 1/11/2017.
 */
public class CukeAppenderFilter extends Filter<ILoggingEvent> {
    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (event.getLoggerName().contains("ExecutionListener"))
            return FilterReply.DENY;
        return FilterReply.ACCEPT;
    }
}
