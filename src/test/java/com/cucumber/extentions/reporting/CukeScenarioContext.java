package com.cucumber.extentions.reporting;

import cucumber.api.Scenario;

/**
 * Created by vdrumea on 1/11/2017.
 */
public class CukeScenarioContext {

    private static CukeScenarioContext instance;

    public static synchronized CukeScenarioContext getInstance() {
        if (instance == null)
            instance = new CukeScenarioContext();
        return instance;
    }

    private CukeScenarioContext() {
    }

    private Scenario scenario;

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public void attachScreenShot(byte[] screenshot){
        scenario.embed(screenshot, "image/png");
    }
}
