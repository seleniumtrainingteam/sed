package com.cucumber.extentions.runner;

import cucumber.api.junit.Cucumber;
import cucumber.runtime.junit.FeatureRunner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

import java.io.IOException;

/**
 * Created by vdrumea on 1/13/2017.
 */
public class CustomCucumberRunner extends Cucumber {
    private static ExecutionListener listener;

    public CustomCucumberRunner(Class clazz) throws InitializationError, IOException {
        super(clazz);
    }

    @Override
    protected void runChild(FeatureRunner child, RunNotifier notifier) {
        if (listener == null) {
            listener= new ExecutionListener();
            notifier.addListener(listener);
        }
        child.run(notifier);
    }
}
