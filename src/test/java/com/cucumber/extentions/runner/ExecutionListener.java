package com.cucumber.extentions.runner;

import com.cucumber.extentions.logging.TestLogHelper;
import gherkin.formatter.model.Scenario;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vdrumea on 1/11/2017.
 */
public class ExecutionListener extends RunListener {

    private Logger logger = LoggerFactory.getLogger(ExecutionListener.class);
    private boolean passed;

    public void testRunStarted(Description description) {
        logger.info("testRunStarted: " + description.toString());
    }

    public void testStarted(Description description) throws Exception {
        if (description.isTest()) {
            logger.info("[ STEP STARTED     ]: " + description.toString());
        } else {
            String innerScenario;
            int line;
            Field privateSerializableField = Description.class.getDeclaredField("fUniqueId");
            privateSerializableField.setAccessible(true);
            Serializable scenarioCandidate = (Serializable) privateSerializableField.get(description);
            Scenario scenario = (Scenario) scenarioCandidate;
            innerScenario = scenario.getName();
            line = scenario.getLine();

            TestLogHelper.stopTestLogging();
            SimpleDateFormat format = new SimpleDateFormat("yyyMMddhhmmss");
            TestLogHelper.startTestLogging(format.format(new Date().getTime()) + "_" + innerScenario + "_" + line);
            logger.info("[ TEST STARTED     ]: " + description);
            logger.info("\r\n");
        }
        passed = true;
    }

    public void testFinished(Description description) throws Exception {
        if (passed) {
            if (description.isTest()) {
                logger.info("[ STEP PASSED       ]: " + description.toString());
            } else {
                logger.info("[ TEST PASSED     ]: " + description.toString() + "\n");
            }
            logger.info("\r\n");
        }
    }

    public void testFailure(Failure failure) throws Exception {
        if (failure.getDescription().isTest()) {
            logger.info("[ STEP FAILED      ]: " + failure.getDescription().toString());
        } else {
            logger.info("[ TEST FAILED      ]: " + failure.getDescription().toString());

            if (failure.getException() instanceof AssertionError) {
                logger.error("[ ASSERTION FAILED  ]: " + failure.getException().getMessage() + "\n");
            } else {
                logger.error("[ EXCEPTION THROWN  ]: " + failure.getException().getMessage() + "\n");
            }
        }
        passed = false;
    }

    public void testIgnored(Description description) throws Exception {
        if (description.isTest()) {
            logger.debug("[ STEP IGNORED     ]: " + description.toString());
        } else {
            logger.debug("[ TEST IGNORED     ]: " + description.toString());
        }
    }
}
