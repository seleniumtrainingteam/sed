package com.openCart.Context;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vdrumea on 12/13/2016.
 */
@Component
public class ScenarioContext {
    private static final Map<Object, Object> data = new HashMap<>();

    public void save(Object key, Object value) {
        data.put(key, value);
    }

    public Object getData(Object key){
        return data.get(key);
    }
}
