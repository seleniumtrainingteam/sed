package com.openCart.Defaults;

import org.openqa.selenium.WebDriver;

/**
 * Created by vdrumea on 3/25/2016.
 */
public abstract class AbstractPage implements Page {


    protected WebDriver driver;
    protected String url;
    protected String name;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public String name() {
        return name;
    }
}
