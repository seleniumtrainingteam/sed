package com.openCart.Defaults;

/**
 * Created by vdrumea on 12/5/2016.
 */
public interface Page {

    String getUrl();
    String name();
}
