package com.openCart.PageObjects;

import com.openCart.Defaults.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * Created by vdrumea on 12/5/2016.
 */
public class HomePage extends AbstractPage {

    Properties environmentProperties;
    private static String url = "http://localhost:1234/shop/index.php";
    private static String name = "Home Page";

    @FindBy(xpath = "//*[@id='search']/input")
    private WebElement searchBox;

    @FindBy(xpath = "//*[@id='search']//button")
    private WebElement searchButton;

    @FindBys(@FindBy(xpath = "//*[@id='content']/div[3]/div"))
    private List<WebElement> products;

    public HomePage(WebDriver driver) throws IOException {
        super(driver);
        driver.manage().window().maximize();
        driver.get(url);
        PageFactory.initElements(driver, this);
    }

    public String getUrl() {
        return url;
    }

    public String name() {
        return name;
    }

    public void searchBy(String value) {
        searchBox.sendKeys(value);
        searchButton.click();
    }

    public List<WebElement> products() {
        return products;
    }

    public WebElement searchBox() {
        return searchBox;
    }

    public WebElement searchButton() {
        return searchButton;
    }
}
