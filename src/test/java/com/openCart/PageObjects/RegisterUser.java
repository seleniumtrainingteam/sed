package com.openCart.PageObjects;

import com.openCart.Defaults.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by ealtuhova on 2/13/2017.
 */
public class RegisterUser extends AbstractPage{
    private static String url = "http://localhost:1234/shop/index.php?route=account/register";

    public RegisterUser(WebDriver driver){
        super(driver);
        driver.manage().window().maximize();
        driver.get(url);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = ".//*[@id='input-firstname']")
    private WebElement firstName;

    @FindBy(xpath = ".//*[@id='input-lastname']")
    private WebElement lastName;

    @FindBy(xpath = ".//*[@id='input-email']")
    private WebElement email;

    @FindBy(xpath = ".//*[@id='input-telephone']")
    private WebElement telephone;

    @FindBy(xpath = ".//*[@id='input-address-1']")
    private WebElement address1;

    @FindBy(xpath = ".//*[@id='input-city']")
    private WebElement city;

    @FindBy(xpath = ".//*[@id='input-postcode']")
    private WebElement postcode;

    @FindBy(xpath = ".//*[@id='input-country']")
    private WebElement country;

    @FindBy(xpath = ".//*[@id='input-zone']")
    private WebElement region;

    @FindBy(xpath = ".//*[@id='input-password']")
    private WebElement password;

    @FindBy(xpath = ".//*[@id='input-confirm']")
    private WebElement passConfirm;

    @FindBy(xpath = ".//*[@id='content']/form/div/div/input[1]")
    private WebElement license;

    @FindBy(xpath = ".//*[@id='content']/form/div/div/input[2]")
    private WebElement submit;

    public String getUrl() {
        return url;
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public WebElement getLastName() {
        return lastName;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getTelephone() {
        return telephone;
    }

    public WebElement getAddress1() {
        return address1;
    }

    public WebElement getCity() {
        return city;
    }

    public WebElement getPostcode() {
        return postcode;
    }

    public WebElement getCountry() {
        return country;
    }

    public WebElement getRegion() {
        return region;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getPassConfirm() {
        return passConfirm;
    }

    public WebElement getLicense() {
        return license;
    }

    public WebElement getSubmit() {
        return submit;
    }
}
