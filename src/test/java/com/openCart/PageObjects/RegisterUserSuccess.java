package com.openCart.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by ealtuhova on 2/13/2017.
 */
public class RegisterUserSuccess {
    private static String url = "http://localhost:1234/shop/index.php?route=account/success";

    @FindBy(xpath = ".//*[@id='content']/h1")
    private WebElement confirmationMessage;

    @FindBy (xpath = ".//*[@id='content']/div/div/a")
    private WebElement continueButton;

    public RegisterUserSuccess(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

    public WebElement getConfirmationMessage(){return confirmationMessage;}

    public WebElement getContinueButton(){return continueButton;}

    public String getUrl(){
        return url;
    }
}
