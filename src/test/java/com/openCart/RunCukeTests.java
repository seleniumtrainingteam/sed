package com.openCart;

import com.cucumber.extentions.runner.CustomCucumberRunner;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by vdrumea on 3/24/2016.
 */
@RunWith(CustomCucumberRunner.class)
@CucumberOptions(
//        strict = false,
//        format = {"pretty", "html:target/html"},
        format = {"pretty","json:target/cucumber.json"},
        features = "src/test/resources",
        tags = {"@test1"},
        glue ={"com/openCart/StepDefinitions"}
)
public class RunCukeTests {
}
