package com.openCart.StepDefinitions;

import com.openCart.Context.ScenarioContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by vdrumea on 12/7/2016.
 */
public class AbstractPageStepDefs {
    protected static WebDriver driver;

//        protected ScenarioContext scenarioContext = new ScenarioContext();
    @Autowired
    protected ScenarioContext scenarioContext;

    protected WebDriver getDriver() {
        if ((driver == null)||(((RemoteWebDriver)driver).getSessionId() == null)) {
            driver = new FirefoxDriver();
        }
        return  driver;
    }
}
