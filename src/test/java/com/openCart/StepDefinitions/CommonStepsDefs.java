package com.openCart.StepDefinitions;

import com.openCart.Defaults.Page;
import com.openCart.PageObjects.HomePage;
import com.openCart.PageObjects.RegisterUser;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.openCart.Context.DataKeys.PAGE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


/**
 * Created by ealtuhova on 12/11/2016.
 */
public class CommonStepsDefs extends AbstractPageStepDefs {

    private Logger logger = LoggerFactory.getLogger(CommonStepsDefs.class.getName());
    WebDriver driver = getDriver();

    @Given("^user is on the (.*) page$")
    public void onPage(String pageName) throws Throwable {
        Page page = null;
        if (pageName.equals("Register User")) {
            page = new RegisterUser(driver);
            assertThat("Assert that I am on " + pageName + " Page", driver.getCurrentUrl(), is(page.getUrl()));
        }
        if (pageName.equals("Home Page")) {
            page = new HomePage(driver);
            assertThat("Assert that I am on " + pageName, driver.getCurrentUrl(), is(page.getUrl()));
        }
        logger.info("The " + pageName + " is opened");
        scenarioContext.save(PAGE, page);
    }
}
