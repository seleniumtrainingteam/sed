package com.openCart.StepDefinitions;

import com.openCart.PageObjects.HomePage;
import com.openCart.Utils.ScreenshotUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.openCart.Context.DataKeys.PAGE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by vdrumea on 12/6/2016.
 */
public class HomePageStepDefs extends AbstractPageStepDefs {

    private Logger logger = LoggerFactory.getLogger(HomePageStepDefs.class.getName());

    @Autowired
    @Qualifier("environmentProperties")
    protected Properties environmentProperties;

    public HomePageStepDefs() throws IOException {
    }

    @When("^I search by (.*)$")
    public void iSearchByProductName(String productName) throws Throwable {
        HomePage page = (HomePage) scenarioContext.getData(PAGE);
        page.searchBy(productName);
        logger.info("Searched by product: " + productName);
        ScreenshotUtils.highlightAndScreenshot(getDriver(), "Search by product", page.searchBox());
    }

    @Then("^(\\d+) products are displayed$")
    public void productsAreDisplayed(int numberOfProducts) throws Throwable {
        HomePage page = (HomePage) scenarioContext.getData(PAGE);
        Thread.sleep(2000);
        assertThat("Assert that the number of products is: " + numberOfProducts, page.products().size(), is(numberOfProducts));
        logger.info("The number of products is: " + numberOfProducts);
        ScreenshotUtils.highlightAndScreenshot(getDriver(), "The number of products", page.products().get(0));
    }
}
