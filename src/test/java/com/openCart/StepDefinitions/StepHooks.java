package com.openCart.StepDefinitions;

import com.cucumber.extentions.reporting.CukeScenarioContext;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;

/**
 * Created by vdrumea on 12/7/2016.
 */
public class StepHooks extends AbstractPageStepDefs{
    WebDriver driver = getDriver();

    @Before(order = 1)
    public void setUp(Scenario scenario){
        getDriver();
        CukeScenarioContext.getInstance().setScenario(scenario);
    }

    @After(order = 1)
    public void quit(){
        driver.quit();
    }
}
