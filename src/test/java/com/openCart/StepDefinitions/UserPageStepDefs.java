package com.openCart.StepDefinitions;

import com.openCart.PageObjects.RegisterUser;
import com.openCart.PageObjects.RegisterUserSuccess;
import cucumber.api.java.en.Then;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


/**
 * Created by ealtuhova on 12/11/2016.
 */
public class UserPageStepDefs extends AbstractPageStepDefs {

    //    WebDriver driver = getDriver();
    RegisterUser page = new RegisterUser(driver);

    @Then("^user is submitting valid parameters into registration form$")
    public void userFillsRegistrationForm(List<String> params) throws InterruptedException {
        Thread.sleep(3000);
        page.getFirstName().sendKeys(params.get(0));
        page.getLastName().sendKeys(params.get(1));
        page.getEmail().sendKeys(params.get(2));
        page.getTelephone().sendKeys(params.get(3));
        page.getAddress1().sendKeys(params.get(4));
        page.getCity().sendKeys(params.get(5));
        page.getCountry().sendKeys(params.get(6));
        Thread.sleep(5000);
        page.getRegion().sendKeys(params.get(7));
        page.getPassword().sendKeys(params.get(8));
        page.getPassConfirm().sendKeys(params.get(9));

        page.getLicense().click();
        page.getSubmit().click();
    }

    @Then("^user is redirected on confirmation page$")
    public void userIsRedirectedConfirm() throws Throwable {
        Thread.sleep(500);
        RegisterUserSuccess page_s = new RegisterUserSuccess(driver);

        assertThat("Assert that I am on confirmation Page", driver.getCurrentUrl(), is(page_s.getUrl()));

        assertThat("Confirmation message is correct", page_s.getConfirmationMessage().getText(),
                is("Your Account Has Been Created!"));

        page_s.getContinueButton().click();

    }

}
