package com.openCart.Utils;

import com.cucumber.extentions.logging.TestLogHelper;
import com.cucumber.extentions.reporting.CukeScenarioContext;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
//import ru.yandex.qatools.ashot.AShot;
//import ru.yandex.qatools.ashot.Screenshot;
//import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;


/**
 * Created by vdrumea on 12/19/2016.
 */

@Component
public class ScreenshotUtils {
    private static Properties environmentProperties;
    private static Logger logger = LoggerFactory.getLogger(ScreenshotUtils.class);
    private static int screenshotIndex = 1;
    private static String currentLogName = "";
    public static String folderPath;


    public ScreenshotUtils() {
    }


    private static void executeJavaScript(WebDriver driver, String script, WebElement... elements) {
        ((JavascriptExecutor) driver).executeScript(script, elements);
    }

    public static void highlightElement(WebDriver driver, WebElement element) throws InterruptedException {
        try {
            Thread.sleep(2000);
            executeJavaScript(driver, "arguments[0].style.border='3px solid red'", element);
        } catch (TimeoutException te) {
            logger.trace("Timeout for highlight element", te);
        }
    }

    public static void unhighlightElement(WebDriver driver, WebElement element) {
        try {
            executeJavaScript(driver, "arguments[0].style.removeProperty('border');", element);
        } catch (TimeoutException te) {
            logger.trace("Timeout for unhighlight element", te);
        }
    }

    public static void makeAScreenshot(WebDriver driver, String filename) throws IOException, AWTException, InterruptedException {
        makeAScreenshot(driver, getCurrentScreenshotFolderPath(), filename);
    }

    public static String getCurrentScreenshotFolderPath() throws IOException {
//        environmentProperties = new Properties();
//        InputStream stream = new FileInputStream("properties/environment.properties");
//        environmentProperties.load(stream);
//        folderPath = environmentProperties.getProperty("folder.path.screenshot");
        String logFilePath = TestLogHelper.getCurrentLogName();
        String logFolderPath = (logFilePath.equals("test")) ? "test" : logFilePath;
        logFolderPath = String.format(folderPath, logFolderPath);
        return logFolderPath;


    }

    public static void makeAScreenshot(WebDriver driver, String directory, String fileName) throws IOException, AWTException, InterruptedException {
        updateScreenshotIndexIfNewTest();
        Thread.sleep(2000);
        String imageFormat = "PNG";
        String imageFileExtension = ".png";
        byte[] imageInByte;


        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(screenSize);
        Robot robot = new Robot();

        final BufferedImage image = robot.createScreenCapture(screenRectangle);
//        final Screenshot screenshot = new AShot().shootingStrategy(
//                new ViewportPastingStrategy(500)).takeScreenshot(driver);
//        final BufferedImage image = screenshot.getImage();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, imageFormat, baos);
        baos.flush();
        imageInByte = baos.toByteArray();

        CukeScenarioContext.getInstance().attachScreenShot(imageInByte);

        Files.createDirectories(Paths.get(directory));
        File file = new File(directory + String.format("%03d", screenshotIndex++) + fileName + imageFileExtension);
        FileOutputStream fos = new FileOutputStream(file);
        try {
            fos.write(imageInByte);
        } finally {
            fos.close();
        }
    }

    private static void updateScreenshotIndexIfNewTest() {
        if (!currentLogName.equals(TestLogHelper.getCurrentLogName())) {
            currentLogName = TestLogHelper.getCurrentLogName();
            screenshotIndex = 1;
        }
    }

    public static void highlightAndScreenshot(WebDriver driver, String filename, WebElement... elements) throws IOException, InterruptedException {
        for (WebElement we : elements) {
            highlightElement(driver, we);
        }
        try {
            makeAScreenshot(driver, filename);
        } catch (AWTException e) {
            e.printStackTrace();
        } finally {
            for (WebElement we : elements) {
                unhighlightElement(driver, we);
            }
        }
    }

    @Value("${folder.path.screenshot}")
    public void setFolderPath(String path) {
        folderPath = path;
    }
}
