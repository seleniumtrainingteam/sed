Feature: Create brand new user

  Scenario:
    Given user is on the Register User page
    When user is submitting valid parameters into registration form
      |Janetta2|Dotte2|email2222@email2.com|+37368000000|str.Socoleni 10|Chisinau|Moldova|Chisinau|Password123-|Password123-|
    Then user is redirected on confirmation page
    Then user is on the My Account page
