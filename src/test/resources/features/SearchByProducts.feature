Feature:

  @test1
  Scenario Outline: Check search functionality
    Given user is on the Home Page page
    When I search by <productName>
    Then <numberOfProducts> products are displayed
    Examples:
      | productName | numberOfProducts |
      | Mac         | 4                |
      | Apple       | 1                |
      | Samsung     | 2                |
      | HTC         | 1                |